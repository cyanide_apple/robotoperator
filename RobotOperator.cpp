#include"RobotOperator.h"
#include<string>
using namespace std;
int RobotOperator::encryptCode(int sayi_) {
	Encryption e;
	sayi_ = e.encrypt(sayi_);
	return sayi_;

}
void RobotOperator::setNameSurname(string a, string b) {
	name = a;
	surname = b;
}

int RobotOperator::decryptCode(int sayi_) {
	Encryption e;
	sayi_ = e.decrypt(sayi_);
	return sayi_;
}
bool RobotOperator::checkAccessCode(int sayi_) {
	sayi_=encryptCode(sayi_);
	if (sayi_ == accessCode) {
		accessState = true;
		return true;
	}
	else {
		return false;
	}
}
void RobotOperator::print() {
	cout << endl << "Name:" << name << endl << "Surname:" << surname << endl << "Access State:";
	if (accessState == true) {
		cout << "true" << endl;
	}
	else {
		cout << "false" << endl;
	}
}