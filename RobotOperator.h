#pragma once
#include"Encryption.h"
#include<iostream>
using namespace std;

class RobotOperator {
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	int encryptCode(int);
	int decryptCode(int);
public:
	RobotOperator(int code) {
		accessCode = encryptCode(code);
		name = "noname";
		surname = "nosurname";
		accessState = false;
	}
	~RobotOperator()
	{

	}
	void setNameSurname(string,string);
	bool checkAccessCode(int);
	void print();
};